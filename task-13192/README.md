Extrapolating network totals from hidden-service statistics
===========================================================

Fetch (and inflate, but not extract) tarballs and/or fetch single files
from CollecTor and store them in the following directories:

  in/collector/archive/relay-descriptors/extra-infos/
  in/collector/archive/relay-descriptors/consensuses/
  in/collector/recent/relay-descriptors/extra-infos/
  in/collector/recent/relay-descriptors/consensuses/

Fetch the latest bandwidth.csv file from Metrics and put it in the
following directory:

  in/metrics/bandwidth.csv

Add metrics-lib to the classpath and compile the classes in src/java/.

Run Java class ExtrapolateHidServStats.

Run the R script:

  R --slave -f src/R/plot.R

